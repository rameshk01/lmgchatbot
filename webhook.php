<?php

$data = json_decode(file_get_contents('php://input'), true);
$fb_sid =  $data['sessionId'];
header('Content-Type: application/json');
$fb_data = [
  "recipient" => [
    "id" => $fb_sid
  ],
  "message" => [
    "attachment" => [
      "type" => "template",
      "payload" =>  [
        "template_type" => "button",
        "text" => "Hi, go to webview",
        "buttons" => [
          [
            "type" => "web_url",
            "url" => "https://www.maxfashion.in",
            "title" => "Show Website",
            "webview_height_ratio" => "full",
            "messenger_extensions" => true,
          ],
        ]
      ]
    ]
  ]
];

const facebookRichData = [
  'attachment' => [
    'type' => 'template',
    'payload' => [
      'template_type' => 'generic',
      'elements' => [
        [
          'title' => 'this is a title',
          'image_url' => 'https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png',
          'subtitle' => 'This is a subtitle',
          'default_action' => [
            'type' => 'web_url',
            'url' => 'https://assistant.google.com/'
          ],
          'buttons' => [
            [
              'type' => 'web_url',
              'url' => 'https://assistant.google.com/',
              'title' => 'This is a button'
            ]
          ]
        ]
      ]
    ]
  ]
];

print json_encode([
  "text" => "hello from dialogflow",
  "speech" => "hello from dialogflow",
  "data" =>  [
    "facebook" => facebookRichData
  ],
]);
